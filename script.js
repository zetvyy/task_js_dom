// Buat Element
const createH2 = document.createElement("h2");
createH2.textContent = "Latihan DOM";

const box = document.querySelector(".box1");
box.append(createH2);

// Hapus Element
const text2 = document.querySelector("#text2");
text2.remove();

// Event
const box1 = document.querySelector(".box1");
const box2 = document.querySelector(".box2");
const inputArea = document.getElementById("input-area");

box1.addEventListener("click", ubahTulisan);
box2.addEventListener("mouseenter", ubahWarna);
inputArea.addEventListener("shiftKey", isKeyPressed);

function ubahTulisan() {
  box1.style.fontSize = "50px";
}

function ubahWarna() {
  box2.style.backgroundColor = "salmon";
}

function isKeyPressed(event) {
  const p = document.getElementById("paragraf");
  if (event.shiftKey) {
    p.innerText = "The SHIFT key was pressed!";
  } else {
    p.innerText = "The SHIFT key was NOT pressed!";
  }
}
